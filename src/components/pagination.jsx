import * as React from 'react';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';

export default function PaginationRounded() {
  return (
    <Stack spacing={2}>
      <Pagination count={10} shape="rounded" id="standard-pages" />
      <Pagination count={10} variant="outlined" shape="rounded" id="outlined-pages" />
      <Pagination count={10} showFirstButton showLastButton id="first-last-buttons" />
      <Pagination count={10} hidePrevButton hideNextButton id="no-first-last-buttons" />
    </Stack>
  );
}
