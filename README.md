# Storybook Component Testing

This repo explores the potential of Storybook's interaction testing (component testing)
feature. It uses standard components from Create-React-App through the Mui testing library:
<https://mui.com/material-ui/getting-started/overview/>

## Interacting with Components

Components can be viewed and interacted with through Storybook, which runs locally on 
port 6006 (see script command for storybook in `package.json`).

You can start Storybook with `npm run storybook` or `yarn storybook`

## Running the tests

Components under test are found in the `/src/components` folder.
Tests are held in `.stories.js` files in the `/src/stories` folder.

Interaction tests with Storybook are run using Jest and Playwright, either with
`npm run <command>` or `yarn <command>`
This will open a new browser window where the tests can be viewed and debugged.

You can also run the tests headlessly with `npm run <command>` or `yarn run <command>`

## Notes

TBC

## Known Issues

None (so far!)
